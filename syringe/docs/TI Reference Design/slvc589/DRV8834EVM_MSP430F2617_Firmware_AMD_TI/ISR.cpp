//******************************************************************************
//  Jose I Quinones
//  Texas Instruments Inc.
//  May 2011
//  Built with IAR Embedded Workbench Version: 5.1
//******************************************************************************

#include "Config.h"

/************************************************************
* Interrupt Vectors (offset from 0xFFC0)
*************************************************************
#define DAC12_VECTOR        (14 * 2u) 0xFFDC DAC12 
#define DMA_VECTOR          (15 * 2u) 0xFFDE DMA
#define USCIAB1TX_VECTOR    (16 * 2u) 0xFFE0 USCI A1/B1 Transmit
#define USCIAB1RX_VECTOR    (17 * 2u) 0xFFE2 USCI A1/B1 Receive
#define PORT1_VECTOR        (18 * 2u) 0xFFE4 Port 1
#define PORT2_VECTOR        (19 * 2u) 0xFFE6 Port 2
#define ADC12_VECTOR        (21 * 2u) 0xFFEA ADC
#define USCIAB0TX_VECTOR    (22 * 2u) 0xFFEC USCI A0/B0 Transmit
#define USCIAB0RX_VECTOR    (23 * 2u) 0xFFEE USCI A0/B0 Receive
#define TIMERA1_VECTOR      (24 * 2u) 0xFFF0 Timer A CC1-2, TA
#define TIMERA0_VECTOR      (25 * 2u) 0xFFF2 Timer A CC0
#define WDT_VECTOR          (26 * 2u) 0xFFF4 Watchdog Timer
#define COMPARATORA_VECTOR  (27 * 2u) 0xFFF6 Comparator A
#define TIMERB1_VECTOR      (28 * 2u) 0xFFF8 Timer B CC1-6, TB
#define TIMERB0_VECTOR      (29 * 2u) 0xFFFA Timer B CC0
#define NMI_VECTOR          (30 * 2u) 0xFFFC Non-maskable
#define RESET_VECTOR        (31 * 2u) 0xFFFE Reset [Highest Priority]*/

#pragma vector=DAC12_VECTOR
__interrupt void DigitaltoAnalogConverter(void)
{

}

#pragma vector=DMA_VECTOR
__interrupt void DigitalToAnalog_DMA(void)
{

}

#pragma vector=USCIAB1TX_VECTOR
__interrupt void USCIAB1_Transmit(void)
{

}

#pragma vector=USCIAB1RX_VECTOR
__interrupt void USCIAB1_Receive(void)
{

}

#pragma vector=PORT1_VECTOR
__interrupt void PORT1_Change(void)
{

}

#pragma vector=PORT2_VECTOR
__interrupt void Port2_Change(void)
{

}

#pragma vector=ADC12_VECTOR
__interrupt void AnalogToDigitalConverter(void)
{

}

#pragma vector=TIMERA1_VECTOR
__interrupt void TimerA1(void)
{

switch (TAIV)
{
case TACCR1_CCIFG_SET:
  break;
case TACCR2_CCIFG_SET:
  break;
case TAIFG_SET:
  break;
}

}


#pragma vector=WDT_VECTOR
__interrupt void Watchdog_Timer(void)
{

}

#pragma vector=COMPARATORA_VECTOR
__interrupt void ComparatorA(void)
{

}

#pragma vector=TIMERB1_VECTOR
__interrupt void TimerB1(void)
{
switch (TBIV)
{
case TBCCR1_CCIFG_SET:
  break;
case TBCCR2_CCIFG_SET:
  
  TBCCR2 += SteppingRateTMR;
  
  if (MoveSteps)
  {
    tmpStepsToMove ++;
    if (tmpStepsToMove == StepsToStop)
      {
      DesiredStepperSpeed = StartingSpeed;
        AccelerateState = STEPS_STOP;  
      }
    else if (tmpStepsToMove == StepsToMove)
      {
      TBCCTL2 &= 0xFF0F;
      MoveSteps = false;
      }
  }
  
  if (MS_Direction) //MS Direction determines whether we move forward (+) or backwards (-) on the lookup table
    {
    MS_Index += MS_Divider;  
    }
  else
    {
    MS_Index -= MS_Divider;  
    }
  
  MS_Index &= INDEX_MASK;                                   //Count from 0 to 2047
  DAC12_0DAT = SineWave[MS_Index & MS_MASK];               
  DAC12_1DAT = SineWave[(MS_Index +  MS_DEPTH) & MS_MASK];  

  int DecayCompute;
  
  DecayCompute = MS_Index & BIT9;
  if (DecayCompute && MS_Direction)
    {
      P4OUT |= DECAYA;           //Fast Decay
    }
  else if (DecayCompute && !MS_Direction)
    {
      P4OUT &= ~DECAYA;           //Slow Decay
    }
  else if (!DecayCompute && MS_Direction)
    {
      P4OUT &= ~DECAYA;           //Slow Decay
    }
  else
    {
      P4OUT |= DECAYA;           //Fast Decay
    }
    
    
  DecayCompute = (MS_Index +  MS_DEPTH) & BIT9;
  if (DecayCompute && MS_Direction)
    {
      P4OUT |= DECAYB;           //Fast Decay
    }
  else if (DecayCompute && !MS_Direction)
    {
      P4OUT &= ~DECAYB;           //Slow Decay
    }
  else if (!DecayCompute && MS_Direction)
    {
      P4OUT &= ~DECAYB;           //Slow Decay
    }
  else
    {
      P4OUT |= DECAYB;           //Fast Decay
    }
  
  int tempOut;
  tempOut = P4OUT;
  tempOut &= ~(BIT4 + BIT3 + BIT2 + BIT1);
  tempOut |= ((SineWave[MS_Index & MS_MASK] ) & 0xF000 ) >> 11; 
  P4OUT = tempOut;
  
  
  break;
case TBCCR3_CCIFG_SET:
  TBCCR0 = TBCCR3 + 80;           //Pulse Width = 5 us
  TBCCR3 += SteppingRateTMR;
  if (MoveSteps)
  {
    tmpStepsToMove ++;
    if (tmpStepsToMove == StepsToStop)
      {
      DesiredStepperSpeed = StartingSpeed;
        AccelerateState = STEPS_STOP;  
      }
    else if (tmpStepsToMove == StepsToMove)
      {
      TBCCTL3 &= 0xFF0F;
      MoveSteps = false;
      }
  }
  break;
case TBCCR4_CCIFG_SET:

  break;
case TBCCR5_CCIFG_SET:
  break;
case TBCCR6_CCIFG_SET:
  break;
case TBIFG_SET:
  break;
}
}

#pragma vector=TIMERB0_VECTOR
__interrupt void TimerB0(void)
{
}

#pragma vector=NMI_VECTOR
__interrupt void NonMaskableInterrupt(void)
{

}

