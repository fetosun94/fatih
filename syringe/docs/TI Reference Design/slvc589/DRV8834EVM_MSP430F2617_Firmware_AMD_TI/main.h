void Task0(void);
void Task1(void);
unsigned int PPS2Timer (unsigned int);

#define     TACCR1_CCIFG_SET    (0x02)
#define     TACCR2_CCIFG_SET    (0x04)
#define     TAIFG_SET           (0x0A)

#define     TBCCR1_CCIFG_SET    (0x02)
#define     TBCCR2_CCIFG_SET    (0x04)
#define     TBCCR3_CCIFG_SET    (0x06)
#define     TBCCR4_CCIFG_SET    (0x08)
#define     TBCCR5_CCIFG_SET    (0x0A)
#define     TBCCR6_CCIFG_SET    (0x0C)
#define     TBIFG_SET           (0x0E)
