//******************************************************************************
//  Jose I Quinones
//  Texas Instruments Inc.
//  June 2009
//  Built with IAR Embedded Workbench Version: 3.42A
//******************************************************************************
#include "Config.h"

void Task1(void)
{

  switch (AccelerateState)
  {
  case (NOACC):
    break;
  case (ACCEL):
    if (tmpAccelTimeBase > 0)
        {
        tmpAccelTimeBase -= 1;                  //Wait until delay expires
        }
    else
        {
        if ((unsigned int) SteppingRate + (unsigned int) AccelerationIncrease > (unsigned int)DesiredStepperSpeed)
        {
          SteppingRateTMR = PPS2Timer(DesiredStepperSpeed);
          AccelerateState = NOACC;
        }
        else
        {
          SteppingRate += AccelerationIncrease;
          SteppingRateTMR = PPS2Timer(SteppingRate);
        }
        tmpAccelTimeBase = AccelTimeBase;
        }
    break;

  case (DECEL):
    if (tmpAccelTimeBase > 0)
        {
        tmpAccelTimeBase -= 1;                  //Wait until delay expires
        }
    else
        {
        if (((unsigned int) SteppingRate - (unsigned int) AccelerationIncrease) < DesiredStepperSpeed)
        {
          AccelerateState = NOACC;
          SteppingRateTMR = PPS2Timer(DesiredStepperSpeed);
        }
        else
        {
          SteppingRate -= AccelerationIncrease;
          SteppingRateTMR = PPS2Timer(SteppingRate);
        }
        tmpAccelTimeBase = AccelTimeBase;
        }
    break;

  case (STOP):
    if (tmpAccelTimeBase > 0)
        {
        tmpAccelTimeBase -= 1;                  //Wait until delay expires
        }
    else
        {
        if (((unsigned int) SteppingRate - (unsigned int) AccelerationIncrease) < DesiredStepperSpeed)
        {
          TBCCTL3 &= 0xFF0F;                                          //Disable CC and Interrupt
          TBCCTL2 &= 0xFF0F;                                          //Disable CC and Interrupt
          AccelerateState = NOACC;

        }
        else
        {
          SteppingRate -= AccelerationIncrease;
          SteppingRateTMR = PPS2Timer(SteppingRate);
        }
        tmpAccelTimeBase = AccelTimeBase;
        }
    break;
    
  case (STEPS_STOP):
    if (tmpAccelTimeBase > 0)
        {
        tmpAccelTimeBase -= 1;                  //Wait until delay expires
        }
    else
        {
        if (((unsigned int) SteppingRate - (unsigned int) AccelerationIncrease) < DesiredStepperSpeed)
        {
          AccelerateState = NOACC;
          SteppingRateTMR = PPS2Timer(DesiredStepperSpeed);

        }
        else
        {
          SteppingRate -= AccelerationIncrease;
          SteppingRateTMR = PPS2Timer(SteppingRate);
        }
        tmpAccelTimeBase = AccelTimeBase;
        }
    break;
  }
}
