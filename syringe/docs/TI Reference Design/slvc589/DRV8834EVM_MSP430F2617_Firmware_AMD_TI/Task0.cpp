#include "Config.h"

int StatusLED;
//Internal Indexer Variable
char AccelerateState;
unsigned int SteppingRate, SteppingRateTMR, StartingSpeed, DesiredStepperSpeed, StoppingSpeed;
unsigned int AccelRate, tmpAccelTimeBase, AccelTimeBase, AccelerationIncrease;
unsigned int StepsToStop, StepsToMove, tmpStepsToMove;
bool MoveSteps, StallDetected, StallDetecting;

//External Indexer Variables
bool MS_Direction;
unsigned long MS_Index;
unsigned int MS_Divider;

void Task0(void)
{
StatusLED += 1;
if (StatusLED == LED_TIMEOUT)
    {
    P6OUT ^= StatusLEDPin;
    StatusLED = 0;
    }  
  
if (MessageComplete)
    {
      SerialOutBuffer[0] = FIRMWARE_REVISION;
      SerialOutBuffer[1] = 0;
      SerialOutBuffer[2] = 0;
      switch(OPCODE)
      {
        
      case (DEVICE_CONFIG):
        P4OUT &= ~NSLEEP;

        switch (SerialBuffer[1])
          {
          case (DUAL_H_BRIDGE):
            P4SEL = (ENABLE_A + PHASE_A + ENABLE_B + PHASE_B);
            TBCCTL1 &= 0xFF1F;
            TBCCTL1 |= TB_OUTMOD_RESSET;
            TBCCTL2 &= 0xFF1F;
            TBCCTL2 |= TB_OUTMOD_RESSET;
            TBCCTL3 &= 0xFF1F;
            TBCCTL3 |= TB_OUTMOD_RESSET;
            TBCCTL4 &= 0xFF1F;
            TBCCTL4 |= TB_OUTMOD_RESSET;
            TBCTL |= TB_CNTL_08;                //Configure Timer to run up to FF (8 bit FRC)
            TBCCTL3 &= ~CCIE;
            TBCCTL2 &= ~CCIE;
            TBCCR0 = 0;
            P1OUT &= ~CONFIG;
            break;
          case (INT_INDEXER):
            P4SEL = STEP;
            TBCTL &= ~TB_CNTL_08;                //Configure Timer to run up to FFFF (16 bit FRC)
            P1OUT |= CONFIG;
            break;
          case (EXT_INDEXER):
            P4SEL = 0;
            TBCTL &= ~TB_CNTL_08;                //Configure Timer to run up to FFFF (16 bit FRC)
            P1OUT &= ~CONFIG;
            break;
          }
        break;
// Write GPIO Data [ OPCODE = 3 ] [ GPIO DATA ] [ Not Used ] [ Not Used ] [ Not Used ]
// GPIO DATA = P13/P12/P11/P10/P47/P46/P45/P44
      case (WRITE_GPIO):
            int tempOut;
            tempOut = P4OUT;                            //P4OUT = GPIO3/GPIO2/GPIO1/GPIO0/TMR3/TMR2/TMR1/TMR0
            tempOut &= 0x0F;                            //Clear 4 MSB in original Output Register
            tempOut |= (SerialBuffer[1] & 0x0F) << 4;   //Received Data Bytes (RDB) 4 MSB cleared. RDB's 4 LSB shifted into 4 MSB
            P4OUT = tempOut;

            tempOut = P1OUT;                            //P1OUT = NU/NU/NU/NU/GPIO7/GPIO6/GPIO5/GPIO4
            tempOut &= 0xF0;                            //Clear 4 LSB in original Output Register
            tempOut |= (SerialBuffer[1] & 0xF0) >> 4;   //Received Data Register (RDB) 4 LSB cleared.
            P1OUT = tempOut;

            SerialOutBuffer[1] = ~SerialBuffer[1];
            break;
// DAC 0 Config and Write [ OPCODE = 5 ] [ Config HB ] [ Config LB ] [ Data HB ] [ Data LB ]
      case (DAC0_UPDATE):
            DAC12_0CTL = 0;
            DAC12_0CTL = (SerialBuffer[1] * 256) + SerialBuffer[2];
            DAC12_0DAT = (SerialBuffer[3] * 256) + SerialBuffer[4];
            break;
// DAC 1 Config and Write [ OPCODE = 6 ] [ Config HB ] [ Config LB ] [ Data HB ] [ Data LB ]
      case (DAC1_UPDATE):
            DAC12_1CTL = 0;
            DAC12_1CTL = (SerialBuffer[1] * 256) + SerialBuffer[2];
            DAC12_1DAT = (SerialBuffer[3] * 256) + SerialBuffer[4];
            break;
            // Case (TMP0): break; //Opcode 7
// Enable PWM [ OPCODE = 0x0B ] [ Timer # ] [ Duty Cycle ] [ Not Used ] [ Not Used ]
      case (ENABLE_PWM):

        switch (SerialBuffer[1])
        {
        case 1:
          if (SerialBuffer[2] == 0xFF)
            {
            TBCCTL1 &= 0xFF1B;                // Clear OUTMODx bits and OUT bit; Configures the PWM output to OUTx value, in this case 0
            TBCCTL1 |= OUT;                  // Set OUT
            }
          else if (SerialBuffer[1] == 0)
            {
            TBCCTL1 &= 0xFF1B;                // Clear OUTMODx bits and OUT bit; Configures the PWM output to OUTx value, in this case 0
            }
          else
            {
            TBCCR1 = SerialBuffer[2];
            tempOut = TBCCTL1;
            tempOut &= 0xFF1F;                // Clear OUTMODx bits; 3 MSB on lower byte
            tempOut |= TB_OUTMOD_RESSET;
            TBCCTL1 = tempOut;    
            }
        break;

        case 2:
          if (SerialBuffer[2] == 0xFF)
            {
            TBCCTL2 &= 0xFF1B;                // Clear OUTMODx bits and OUT bit; Configures the PWM output to OUTx value, in this case 0
            TBCCTL2 |= OUT;                  // Set OUT
            }
          else if (SerialBuffer[1] == 0)
            {
            TBCCTL2 &= 0xFF1B;                // Clear OUTMODx bits and OUT bit; Configures the PWM output to OUTx value, in this case 0
            }
          else
            {
            TBCCR2 = SerialBuffer[2];
            tempOut = TBCCTL2;
            tempOut &= 0xFF1F;                // Clear OUTMODx bits; 3 MSB on lower byte
            tempOut |= TB_OUTMOD_RESSET;
            TBCCTL2 = tempOut;    
            }
        break;

        case 3:
          if (SerialBuffer[2] == 0xFF)
            {
            TBCCTL3 &= 0xFF1B;                // Clear OUTMODx bits and OUT bit; Configures the PWM output to OUTx value, in this case 0
            TBCCTL3 |= OUT;                  // Set OUT
            }
          else if (SerialBuffer[1] == 0)
            {
            TBCCTL3 &= 0xFF1B;                // Clear OUTMODx bits and OUT bit; Configures the PWM output to OUTx value, in this case 0
            }
          else
            {
            TBCCR3 = SerialBuffer[2];
            tempOut = TBCCTL3;
            tempOut &= 0xFF1F;                // Clear OUTMODx bits; 3 MSB on lower byte
            tempOut |= TB_OUTMOD_RESSET;
            TBCCTL3 = tempOut;    
            }
        break;

        case 4:
          if (SerialBuffer[2] == 0xFF)
            {
            TBCCTL4 &= 0xFF1B;                // Clear OUTMODx bits and OUT bit; Configures the PWM output to OUTx value, in this case 0
            TBCCTL4 |= OUT;                  // Set OUT
            }
        else if (SerialBuffer[1] == 0)
            {
            TBCCTL4 &= 0xFF1B;                // Clear OUTMODx bits and OUT bit; Configures the PWM output to OUTx value, in this case 0
            }
         else
            {
            TBCCR4 = SerialBuffer[2];
            tempOut = TBCCTL4;
            tempOut &= 0xFF1F;                // Clear OUTMODx bits; 3 MSB on lower byte
            tempOut |= TB_OUTMOD_RESSET;
            TBCCTL4 = tempOut;    
            }
        break;
        }
      break;
// Update PWM [ OPCODE = 0x0D ] [ Timer # ] [ Duty Cycle ] [ Not Used ] [ Not Used ]
      case (UPDATE_PWM):
        switch (SerialBuffer[1])
        {
        case 1:
            TBCCR1 = SerialBuffer[2];
        break;
        case 2:
            TBCCR2 = SerialBuffer[2];
        break;
        case 3:
            TBCCR3 = SerialBuffer[2];
        break;
        case 4:
            TBCCR4 = SerialBuffer[2];
        break;
        }
            break;

// Pulse Timer Output [ OPCODE = 0x0F ] [Timer Used] [ Pulse Length Hi ] [ Pulse Length Lo ] [ Not Used ]
      case (PULSE_STEP):
        TBCTL &= ~TB_CNTL_08;                //Configure Timer to run up to FFFF (16 bit FRC)
        switch (SerialBuffer[1])
        {
        case 3:
            TBCCTL3 &= 0xFF1F;                                          // Clear OUTMODx bits; 3 MSB on lower byte; Timer configured to output mode
            TBCCTL3 |= TB_OUT_HIGH;                                     //Set the output (pulse start)
            TBCCTL3 |= TB_OUTMOD_RESET;                                 //Configure the timer to reset
            TBCCR3 = TBR + ((SerialBuffer[2] * 256) + SerialBuffer[3]); //Configure the pulse reset time (pulse end)
          break;
        }
        break;

// START STEPPER [ OPCODE = 0x10 ] [ Frequency Hi ] [ Frequency Lo ] [ Accel Rate Hi ] [ Accel Rate Lo]
      case (START_STEPPER):
            TBCCTL3 &= 0xFF1F;                                          // Clear OUTMODx bits; 3 MSB on lower byte; Timer configured to output mode
            TBCCTL3 |= TB_OUTMOD_SETRES + CCIE;                         //Configure the timer to SET. TBCCR0 does the reset
            DesiredStepperSpeed = (SerialBuffer[1] * 256) + SerialBuffer[2];   //Configure the Frequency Rate
            AccelRate = (SerialBuffer[3] * 256) + SerialBuffer[4];

            if (AccelRate <= ACCEL_RATE_COUNT)
              {
              AccelTimeBase = ACCEL_RATE_COUNT/AccelRate;
              AccelerationIncrease = 1;
              }
            else
              {
              AccelTimeBase = 0;
              AccelerationIncrease = AccelRate/ACCEL_RATE_COUNT;  
              }
              tmpAccelTimeBase = AccelTimeBase;
            
            SteppingRate = StartingSpeed;
            SteppingRateTMR = PPS2Timer(StartingSpeed);
            TBCCR3 = SteppingRateTMR;
            AccelerateState = ACCEL;
        break;
// STOP STEPPER [ OPCODE = 0x11 ] [ Not Used ] [ Not Used ] [ Accel Rate Hi ] [ Accel Rate Lo]
      case (STOP_STEPPER):
            AccelRate = (SerialBuffer[3] * 256) + SerialBuffer[4];

            if (AccelRate <= ACCEL_RATE_COUNT)
              {
              AccelTimeBase = ACCEL_RATE_COUNT/AccelRate;
              AccelerationIncrease = 1;
              }
            else
              {
              AccelTimeBase = 0;
              AccelerationIncrease = AccelRate/ACCEL_RATE_COUNT;  
              }
              tmpAccelTimeBase = AccelTimeBase;
            
            AccelerateState = STOP;
            DesiredStepperSpeed = StoppingSpeed;                               //Speed to stop at
      break;
// STEPPER_SPEED [ OPCODE = 0x12 ] [ Frequency Hi ] [ Frequency Lo ] [ Accel Rate ] [ Accel Time Base ]
      case (STEPPER_SPEED):
            DesiredStepperSpeed = (SerialBuffer[1] * 256) + SerialBuffer[2];   //Configure the Frequency Rate
            AccelRate = (SerialBuffer[3] * 256) + SerialBuffer[4];

            if (AccelRate <= ACCEL_RATE_COUNT)
              {
              AccelTimeBase = ACCEL_RATE_COUNT/AccelRate;
              AccelerationIncrease = 1;
              }
            else
              {
              AccelTimeBase = 0;
              AccelerationIncrease = AccelRate/ACCEL_RATE_COUNT;  
              }
              tmpAccelTimeBase = AccelTimeBase;

            if ((unsigned int)DesiredStepperSpeed > (unsigned int)SteppingRate)
            {
                AccelerateState = ACCEL;
            }
            else
            {
                AccelerateState = DECEL;
            }
      break;
// MOVE_STEPS [ OPCODE = 0x13 ] [ Frequency Hi ] [ Frequency Lo ] [ Accel Rate Hi ] [ Accel Rate Lo ]
      case (MOVE_STEPS):
            TBCCTL3 &= 0xFF1F;                                          // Clear OUTMODx bits; 3 MSB on lower byte; Timer configured to output mode
            TBCCTL3 |= TB_OUTMOD_SETRES + CCIE;                         //Configure the timer to SET. TBCCR0 does the reset
            DesiredStepperSpeed = (SerialBuffer[1] * 256) + SerialBuffer[2];   //Configure the Frequency Rate
            AccelRate = (SerialBuffer[3] * 256) + SerialBuffer[4];

            if (AccelRate <= ACCEL_RATE_COUNT)
              {
              AccelTimeBase = ACCEL_RATE_COUNT/AccelRate;
              AccelerationIncrease = 1;
              }
            else
              {
              AccelTimeBase = 0;
              AccelerationIncrease = AccelRate/ACCEL_RATE_COUNT;  
              }
              tmpAccelTimeBase = AccelTimeBase;
            
            SteppingRate = StartingSpeed;
            SteppingRateTMR = PPS2Timer(StartingSpeed);
            TBCCR3 = SteppingRateTMR;
            AccelerateState = ACCEL;
            tmpStepsToMove = 0;
            MoveSteps = true;
           
            break;
// STEPPER_CONFIG [ OPCODE = 0x14 ] [ Frequency Hi ] [ Frequency Lo ] [ Not Used ] [ Not Used ]
      case (STEPPER_CONFIG):
            StartingSpeed = (SerialBuffer[1] * 256) + SerialBuffer[2];   //Configure the Frequency Rate
            StoppingSpeed = (SerialBuffer[3] * 256) + SerialBuffer[4];   //Configure the Frequency Rate
            break;
// CONFIG_STEPS [ OPCODE = 0x15 ] [ Number Of Steps Hi] [ Number Of Steps Lo] [ Steps to Stop Hi] [ Steps to Stop Lo]
      case (CONFIG_STEPS):
            StepsToMove = (SerialBuffer[1] * 256) + SerialBuffer[2];   //Configure the Frequency Rate
            StepsToStop = (SerialBuffer[3] * 256) + SerialBuffer[4];   //Configure the Frequency Rate
            break;          

// Write GPIO STEPPER [ OPCODE = 0x16 ] [ GPIO DATA ] [ Not Used ] [ Not Used ] [ Not Used ]
// GPIO DATA = P13/P12/P11/P10/P47/P46/P45/P44
      case (WRITE_GPIO_STEPPER):
            P4OUT = SerialBuffer[1];
            switch (SerialBuffer[2])
            {
              case     FULL_STEP: //00
                P1OUT &= ~MODE1;
                P1DIR |= MODE1;
                P4OUT &= ~MODE0;
                P4DIR |= MODE0;
                break;
              case     HALF_STEP: //01
                P1OUT &= ~MODE1;
                P1DIR |= MODE1;
                P4OUT |= MODE0;
                P4DIR |= MODE0;
                break;
              case     QUAD_STEP: //0Z
                P1OUT &= ~MODE1;
                P1DIR |= MODE1;
                P4DIR &= ~MODE0;    //High Impedance - Input
                break;
              case     EIGHT_STEP: //10
                P1OUT |= MODE1;
                P1DIR |= MODE1;
                P4OUT &= ~MODE0;
                P4DIR |= MODE0;
                break;
              case     TH16_STEP: //11
                P1OUT |= MODE1;
                P1DIR |= MODE1;
                P4OUT |= MODE0;
                P4DIR |= MODE0;
                break;
              case     TH32_STEP: //1Z
                P1OUT |= MODE1;
                P1DIR |= MODE1;
                P4DIR &= ~MODE0;
                break;
            }
            break;             

/*******************************************************************************
// External Microstepping Code
*******************************************************************************/            

// START STEPPER [ OPCODE = 0x20 ] [ Frequency Hi ] [ Frequency Lo ] [ Accel Rate HI ] [ Accel Rate LO ]
      case (MS_START_STEPPER):
            TBCCTL2 &= 0xFF1F;                                          // Clear OUTMODx bits; 3 MSB on lower byte; Timer configured to output mode
            TBCCTL2 |= CCIE;
            DesiredStepperSpeed = (SerialBuffer[1] * 256) + SerialBuffer[2];   //Configure the Frequency Rate
            AccelRate = (SerialBuffer[3] * 256) + SerialBuffer[4];

            if (AccelRate <= ACCEL_RATE_COUNT)
              {
              AccelTimeBase = ACCEL_RATE_COUNT/AccelRate;
              AccelerationIncrease = 1;
              }
            else
              {
              AccelTimeBase = 0;
              AccelerationIncrease = AccelRate/ACCEL_RATE_COUNT;  
              }
              tmpAccelTimeBase = AccelTimeBase;
            
            SteppingRate = StartingSpeed;
            SteppingRateTMR = PPS2Timer(StartingSpeed);
            TBCCR2 = SteppingRateTMR;
            AccelerateState = ACCEL;
        break;
// STOP STEPPER [ OPCODE = 0x21 ] [ Not Used ] [ Not Used ] [ Accel Rate HI ] [ Accel Rate LO ]
      case (MS_STOP_STEPPER):
            AccelRate = (SerialBuffer[3] * 256) + SerialBuffer[4];

            if (AccelRate <= ACCEL_RATE_COUNT)
              {
              AccelTimeBase = ACCEL_RATE_COUNT/AccelRate;
              AccelerationIncrease = 1;
              }
            else
              {
              AccelTimeBase = 0;
              AccelerationIncrease = AccelRate/ACCEL_RATE_COUNT;  
              }
              tmpAccelTimeBase = AccelTimeBase;
            
            AccelerateState = STOP;
            DesiredStepperSpeed = StoppingSpeed;
      break;
// STEPPER_SPEED [ OPCODE = 0x22 ] [ Frequency Hi ] [ Frequency Lo ] [ Accel Rate  Hi] [ Accel Rate  Lo ]
      case (MS_STEPPER_SPEED):
            DesiredStepperSpeed = (SerialBuffer[1] * 256) + SerialBuffer[2];   //Configure the Frequency Rate
            AccelRate = (SerialBuffer[3] * 256) + SerialBuffer[4];

            if (AccelRate <= ACCEL_RATE_COUNT)
              {
              AccelTimeBase = ACCEL_RATE_COUNT/AccelRate;
              AccelerationIncrease = 1;
              }
            else
              {
              AccelTimeBase = 0;
              AccelerationIncrease = AccelRate/ACCEL_RATE_COUNT;  
              }
              tmpAccelTimeBase = AccelTimeBase;

            if ((unsigned int)DesiredStepperSpeed > (unsigned int)SteppingRate)
            {
                AccelerateState = ACCEL;
            }
            else
            {
                AccelerateState = DECEL;
            }
      break;
// MOVE_STEPS [ OPCODE = 0x23 ] [ Frequency Hi ] [ Frequency Lo ] [ Accel Rate Hi ] [ Accel Rate Lo ]
      case (MS_MOVE_STEPS):
            TBCCTL2 &= 0xFF1F;                                          // Clear OUTMODx bits; 3 MSB on lower byte; Timer configured to output mode
            TBCCTL2 |= CCIE;                         //Configure the timer to SET. TBCCR0 does the reset
            DesiredStepperSpeed = (SerialBuffer[1] * 256) + SerialBuffer[2];   //Configure the Frequency Rate
            AccelRate = (SerialBuffer[3] * 256) + SerialBuffer[4];

            if (AccelRate <= ACCEL_RATE_COUNT)
              {
              AccelTimeBase = ACCEL_RATE_COUNT/AccelRate;
              AccelerationIncrease = 1;
              }
            else
              {
              AccelTimeBase = 0;
              AccelerationIncrease = AccelRate/ACCEL_RATE_COUNT;  
              }
              tmpAccelTimeBase = AccelTimeBase;
            
            SteppingRate = StartingSpeed;
            SteppingRateTMR = PPS2Timer(StartingSpeed);
            TBCCR2 = SteppingRateTMR;
            AccelerateState = ACCEL;
            tmpStepsToMove = 0;
            MoveSteps = true;
           
            break;
// STEPPER_CONFIG [ OPCODE = 0x24 ] [ Frequency Hi ] [ Frequency Lo ] [ Not Used ] [ Not Used ]
      case (MS_STEPPER_CONFIG):
            StartingSpeed = (SerialBuffer[1] * 256) + SerialBuffer[2];   //Configure the Frequency Rate
            StoppingSpeed = (SerialBuffer[3] * 256) + SerialBuffer[4];   //Configure the Frequency Rate
            break;
// CONFIG_STEPS [ OPCODE = 0x25 ] [ Number Of Steps Hi] [ Number Of Steps Lo] [ Steps to Stop Hi] [ Steps to Stop Lo]
      case (MS_CONFIG_STEPS):
            StepsToMove = (SerialBuffer[1] * 256) + SerialBuffer[2];   //Configure the Frequency Rate
            StepsToStop = (SerialBuffer[3] * 256) + SerialBuffer[4];   //Configure the Frequency Rate
            break;
// CONFIG_MSTEPS [ OPCODE = 0x26 ] [ Microstepping Resolution] [ Direction ] [ Not Used ] [ Not Used ]
      case (MS_CONFIG_MSTEPS):
            MS_Divider = 1 << SerialBuffer[2];
            MS_Direction = SerialBuffer[1];
            break;             
            
// Read Memory [Opcode = 0xE0 ] [ Address Hi ] [ Address Lo ] [ Not Used ] [ Not Used ]
      case (READ_MEM):
       int * MyPointer;
       int Address;

       Address = (SerialBuffer[1]*256 + SerialBuffer[2]);
       MyPointer = (int *) Address;
       SerialOutBuffer[1] = (*MyPointer & 0xFF00) >> 8;
       SerialOutBuffer[2] = (*MyPointer & 0xFF);
      break;
// Write Memory [Opcode = 0xE1 ] [ Address Hi ] [ Address Lo ] [ Data Hi ] [ Data Lo ]
      case (WRITE_WMEM):
        int Data;

        Address = (SerialBuffer[1]*256 + SerialBuffer[2]);
        Data = (SerialBuffer[3]*256 + SerialBuffer[4]);
        MyPointer = (int *) Address;
        *MyPointer = Data;
        break;

// Write Byte Memory [Opcode = 0xE2 ] [ Address ] [ Address Lo ] [ Data ] [Not Used ]
      case (WRITE_BMEM):
        char BData;
        char * MyPtr;

        Address = (SerialBuffer[1]*256 + SerialBuffer[2]);
        BData = SerialBuffer[3];
        MyPtr = (char *) Address;
        *MyPtr = BData;
        break;

      case (RESET_MCU):
        WDTCTL = 0x00;
        break;
      case (SHOW_CLKS):
        P5SEL |= SerialBuffer[1] & 0xF0;
        break;
      }
    MessageComplete = false;
    SerialOutPointer = 0;
    UCA0TXBUF = SerialOutBuffer[SerialOutPointer];
    IE2 |= UCA0TXIE;
    }
}

unsigned int PPS2Timer (unsigned int PPSValue)
  {
  int TimerValue;
  TimerValue = 8000000/PPSValue;
  return TimerValue;
  }



