//***********************************************************************************
// App Extern Globals                                                               *
//***********************************************************************************
extern char AccelerateState;
extern unsigned int SteppingRate, SteppingRateTMR, StartingSpeed, DesiredStepperSpeed, StoppingSpeed;
extern unsigned int AccelRate, tmpAccelTimeBase, AccelTimeBase, AccelerationIncrease;
extern unsigned int StepsToStop, StepsToMove, tmpStepsToMove;
extern bool MoveSteps, StallDetected, StallDetecting;

//External Indexer Variables
extern bool MS_Direction;
extern unsigned long MS_Index;
extern unsigned int MS_Divider;
//extern const int SineWave[2048];
//***********************************************************************************
// Firmware Revision                                                                *
//***********************************************************************************
#define     FIRMWARE_REVISION   1
//***********************************************************************************
// Application Specific Hardware Definitions                                        *
//***********************************************************************************
#define     StatusLEDPin    0x20

#define     DUAL_H_BRIDGE   0x00
#define     INT_INDEXER     0x01
#define     EXT_INDEXER     0x02

#define     ENABLE_A        BIT1
#define     PHASE_A         BIT2
#define     ENABLE_B        BIT3
#define     PHASE_B         BIT4

#define     NENABLE         BIT1
#define     MODE0           BIT2
#define     STEP            BIT3
#define     DIR             BIT4
#define     NSLEEP          BIT5
#define     DECAYA          BIT6
#define     DECAYB          BIT7
#define     MODE1           BIT0
#define     CONFIG          BIT1

//***********************************************************************************
// Application Specific Definitions                                                 *
//***********************************************************************************
#define     OPCODE      SerialBuffer[0]

#define     NOACC       0
#define     ACCEL       1
#define     DECEL       2
#define     STOP        3
#define     STEPS_STOP  4

#define     FULL_STEP   0
#define     HALF_STEP   1
#define     QUAD_STEP   2
#define     EIGHT_STEP  3
#define     TH16_STEP   4
#define     TH32_STEP   5

#define     ACCEL_RATE_COUNT  4000

#define     LED_TIMEOUT 4000

#define     TABLE_DEPTH 2048
#define     INDEX_MASK  2047
#define     DAC_MASK    0xFFF     // 12 Bits
#define     MS_DEPTH    0x200     // 512 microsteps
#define     MS_MASK     0x7FF
//***********************************************************************************
// Application Specific Opcodes                                                     *
//***********************************************************************************
#define     DEVICE_CONFIG   0x00
#define     WRITE_GPIO      0x01
#define     DAC0_UPDATE     0x02
#define     DAC1_UPDATE     0x03
#define     ENABLE_PWM      0x04
#define     UPDATE_PWM      0x05
#define     PULSE_STEP      0x06

#define     START_STEPPER   0x10
#define     STOP_STEPPER    0x11
#define     STEPPER_SPEED   0x12
#define     MOVE_STEPS      0x13
#define     STEPPER_CONFIG  0x14
#define     CONFIG_STEPS    0x15
#define     WRITE_GPIO_STEPPER 0x16

#define     MS_START_STEPPER   0x20
#define     MS_STOP_STEPPER    0x21
#define     MS_STEPPER_SPEED   0x22
#define     MS_MOVE_STEPS      0x23
#define     MS_STEPPER_CONFIG  0x24
#define     MS_CONFIG_STEPS    0x25
#define     MS_CONFIG_MSTEPS   0x26


//Memory Access Opcodes
#define     READ_MEM        0xE0
#define     WRITE_WMEM      0xE1
#define     WRITE_BMEM      0xE2

// System Opcodes
#define     GET_FW_REV      0xF0
#define     RESET_MCU       0xF1
#define     SHOW_CLKS       0xF2
#define     I2C_FREQ_SEL    0xF3

//***********************************************************************************
// Application Specific Externs                                                     *
//***********************************************************************************

extern int StatusLED;
